import argparse
import math
import os
import h5py
import numpy as np
import scipy
import scipy.ndimage
import scipy.signal
from skimage.morphology import disk
from skimage.filters.rank import mean
#from skimage.transform import rotate
import multiprocessing
import cv2
import time 
from   .  import gpu_med
import matplotlib.pyplot as plt


from aspect_phase import aspect_phase_retrieval as spct
from scipy.ndimage import gaussian_filter

def get_arguments():
    parser = argparse.ArgumentParser(
        description="Produces doubleflats as a function of time."
    )
    parser.add_argument(
        "--scan_file",
        required=True,
        help="the name root of the scan for which the double-flats are generated",
        default="",
    )
    parser.add_argument(
        "--output_file",
        required=True,
        help="the name of the output file",
        default="",
    )
    parser.add_argument(
        "--force_overwrite",
        action="store_true",
        help="will force overwriting if the output target already exists",
    )
    parser.add_argument(
        "--do_median",
        action="store_true",
        help="subtract median for every radiography instead of the blurs for the final averages",
    )
    parser.add_argument(
        "--entry_name",
        required=False,
        help="The entry name",
        default="entry0000",
    )

    parser.add_argument(
        "--period_nturns",
        required=True,
        type=int,
        help="Doubles are produced for periods  n_turns wide",
    )
    parser.add_argument(
        "--proj_step",
        required=False,
        type=int,
        default=1,
        help="take one projection every proj_step. Optional, by default it is 1 ( all projections are summe)",
    )

    parser.add_argument(
        "--sigma_pix",
        required=True,
        type=float,
        help="The sigma in pixels for the high-pass filter",
    )

    parser.add_argument(
        "--nsectors_per_turn",
        required=False,
        type=int,
        default=1,
        help="",
    )
    parser.add_argument(
        "--output_dir_for_filtered",
        required=False,
        type=str,
        default="filtered_dir",
        help="The directory where filtered projections will be written",
    )
    parser.add_argument(
        "--ncpus",
        required=False,
        type=int,
        default=32,
        help="How many processes will be spawned. If not given, this number will be read from SLURM_CPUS_PER_TASK",
    )
    parser.add_argument(
        "--method",
        required=False,
        type=int,
        default=2,
        help="method 1: Alessandro; method 2 Vincent",
    )
    parser.add_argument(
        "--multi_angle_med",
        required=False,
        type=int,
        default=0,
        help="If set to 1, will trigger the multi angle median correction",
    )
    parser.add_argument(
        "--segmentation_threshold",
        required=False,
        type=float,
        default=0,
        help="If set to a value >0, will threshold the data",
    )
    

    args = parser.parse_args()

    f_scan = h5py.File(args.scan_file, "r")
    f_flats = h5py.File(os.path.splitext(args.scan_file)[0] + "_flats.hdf5", "r")
    f_darks = h5py.File(os.path.splitext(args.scan_file)[0] + "_darks.hdf5", "r")

    control_keys = f_scan[f"/{args.entry_name}/instrument/detector/image_key"][()]
    args.angles = f_scan[f"/{args.entry_name}/sample/rotation_angle"][()]
    
    args.angles =   args.angles [np.equal(0, control_keys)]
    
    args.angles = np.unwrap(
        args.angles, period=360
    )

    args.angles = abs(args.angles - args.angles[0])

    # for angle in args.angles:
    #     print(angle)

    args.radios_indexes = np.arange(control_keys.shape[0])[np.equal(0, control_keys)]

    args.radios_current = f_scan[f"/{args.entry_name}/control/data"][()]
    if len( args.radios_current ) > len( control_keys ) :
        print(" WARNING: INCONSISTENCY BETWEEN THE LENGHT OF CURRENT AND LENGHT OF DATA. PROBABLY A H52NX CONVERSION PROBLEM DUE TO SOME WIERDNESS IN THE DATA. NOW CUTTING")
        args.radios_current  = args.radios_current [:len( control_keys ) ]
        
    args.radios_current = args.radios_current[
        np.equal(0, control_keys)
    ]

    g_flats = f_flats[f"/{args.entry_name}/flats"]

    args.flats_keys = [int(k) for k in g_flats.keys() if k.isnumeric()]

    args.flats_keys.sort()

    args.flats_current = g_flats["machine_electric_current"][()]
    
    g_darks = f_darks[f"/{args.entry_name}/darks"]
    for key in g_darks.keys():
        args.dark = g_darks[key][()]
        break

    args.flats = np.array(
        [
            (g_flats[str(k)][()] - args.dark) * 0.2 / args.flats_current[i]
            for i, k in enumerate(args.flats_keys)
        ]
    )

    args.angular_margin = 0

    args.good_ones = np.less_equal(args.angular_margin, args.angles) * np.less_equal(
        args.angles, args.angles[-1] - args.angular_margin
    )

    args.first_to_be_summed = np.searchsorted(args.angles,  args.angular_margin   )
    args.last_to_be_summed = min(  np.searchsorted(args.angles, args.angles[-1] - args.angular_margin  ) , len(args.angles) - 1  ) 

    flats_steps = np.abs(np.diff( args.flats_keys  ) )

    # *****
    # by sector means that several flats are present ( in the data that we are processing)
    # between 0 and 360 ( beginning of the scan)
    # and again several flats between last_scan_num*360 and (last_scan_num+1)*360, but nothing in the middle
    #
    args.flats_by_sector = False   # by default
    
    if len(flats_steps) > 5:
        
        typical_step = np.median(flats_steps)

        # print(" typical ", typical_step)
        for i, d in enumerate(flats_steps):
            # print( i,   args.flats_keys[i+1] - args.flats_keys[i]     ) 
            if d > 2 * typical_step:
                args.flats_by_sector = True
                # read comment above @ # *****
                # args.ndx_first_period = args.radios_indexes[i]

                args.ndx_first_period = args.flats_keys[i]
                args.ndx_last_period = args.flats_keys[i + 1]

                
                for k,c in enumerate(control_keys):
                    if int(c) == 0:
                        first_start_ndx = k
                        break



                first_interval_ndx = None
                second_start_ndx = None
                for k,c in enumerate(control_keys[first_start_ndx:]):
                    if c:
                        first_interval_ndx = k + first_start_ndx
                        break

                if first_interval_ndx is not None:
                    for k,c in enumerate(control_keys[first_interval_ndx:]):
                        if c==0:
                            second_start_ndx = first_interval_ndx + k
                            break

                if second_start_ndx is not None:
                    args.ndx_period = second_start_ndx - first_start_ndx
                else:
                    args.ndx_period = args.ndx_first_period - args.flats_keys[0]
                    


                # print( " FIRST LAST ", args.ndx_first_period,  args.ndx_period, args.ndx_last_period)
            
    # open momentarily the data so that get_radio function, which uses it , can work
    args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]
    args.one_radio_shape = tuple(get_radio_or_flat(args, 0).shape)


    # Then I close all the hdf5 objects. They cannot be serialised and this would prevent
    # the possibility of passing args to the multiprocessing Pool object
    # They will be reopened, if needed, by the multiprocessing processes
    args.d_radios=None
    args.f_scan = None
    args.f_flats  = None
    args.f_darks  = None

    return args


def get_radio_or_flat(args, i, get_flat=False):

    if (not get_flat) and (i >= args.radios_indexes.shape[0]):
        raise RuntimeError(f"Cannot provide radio number {i}  for a radio_indexes shape of lenght {args.radios_indexes.shape[0]}")

    if not get_flat:
        ndx = args.radios_indexes[i]
    else:
        ndx = i

    
    if (
        (not args.flats_by_sector)
        or (ndx <= args.ndx_first_period)
        or (ndx >= args.ndx_last_period)
        or get_flat
    ):
        # this first clause is a simple interpolation for the flats.
        # And interpolation is done for the flats
        # at position ndx
        # This option is triggered either for getting just a flat ( no radio)
        # or, when getting a flattened radio when we are in an intepolating simple region.
        # The region is always simpe if args.flats_by_sector is false
        # while if it is true the reion is simple only if we are in the middle region of the scan
        # between the first turn and the last turn both excluded.
        #
        pos = np.searchsorted(args.flats_keys, ndx)

        if pos == 0:
            i1 = 0
            i2 = 0
            f1 = 1
            f2 = 0
        elif pos == len(args.flats_keys):
            i1 = len(args.flats_keys) - 1
            i2 = 0
            f1 = 1
            f2 = 0
        else:
            i1 = pos - 1
            i2 = pos

            key_i1 = args.flats_keys[i1]
            key_i2 = args.flats_keys[i2]

            f1 = (key_i2 - ndx) / (key_i2 - key_i1)
            f2 = (ndx - key_i1) / (key_i2 - key_i1)

        flat = f1 * args.flats[i1] + f2 * args.flats[i2]

        if get_flat:
            return flat

        radio = args.d_radios[ndx] - args.dark
        
        radio_current = args.radios_current[i]
        radio = radio * 0.2 / radio_current

        return (radio / flat).astype("f")
    else:

        # assuming ndx=i for concatenated scan with flats by sector
        # The scan is supposed to come from night-rail workflow which used
        # the proper tool in nxtomomil in order to concatenat with flats by sectors
        # and in this case (in the nxtomomill method called by night_rail)
        # the layout of the radios is done without holes in the middle
        # so that ndx==i and the flats are given only for the first and the last turn.
        # To be more clear: the scan come for a reference scan at the beginning and a reference scan at the end

        # pi1 is the angular-interpolation point in the middle of the references for the beginning of the scan        
        pi1 = ndx
        while pi1 > args.ndx_first_period:
            pi1 -= args.ndx_period

        # pi2 is the angular-interpolation point in the middle of the references for the end of the scan        
        pi2 = ndx
        while pi2 < args.ndx_last_period:
            pi2 += args.ndx_period

        ff1 = get_radio_or_flat(args, pi1, get_flat=True)
        ff2 = get_radio_or_flat(args, pi2, get_flat=True)

        flat = (ff1 * (pi2 - ndx) + (ndx - pi1) * ff2) / (pi2 - pi1)

        radio = args.d_radios[ndx] - args.dark
        radio_current = args.radios_current[i]

        ## 0.2 means 200 milliAmps, the nominal current for which everything is going to be rescaled
        radio = radio * 0.2 / radio_current

        return (radio / flat).astype("f")

def main():

    args = get_arguments()

    if not args.force_overwrite:
        if os.path.exists(args.output_file):
            print(f"get_doubles_mean: target file {args.output_file} already existing and force_overwriting is false, skipping...")
            return 0
        
    total_useful_turns = int(
        math.ceil(
            (args.angles[args.last_to_be_summed] - args.angles[args.first_to_be_summed])
            / 360
        )
    )

    args.total_useful_turns = total_useful_turns

    args.period_nturns = min(  total_useful_turns, args.period_nturns )
    
    print("Total number of projections: ", args.last_to_be_summed) 
    proj_per_turn = math.ceil(args.last_to_be_summed / total_useful_turns)
    print("Total useful turns: ", total_useful_turns, "proj per turn: ", proj_per_turn)    
    
    if args.method == 1:
        # variables for Alessandro's method
        n_periods = int(math.ceil(total_useful_turns / args.period_nturns))
        ndoubles = n_periods * args.nsectors_per_turn
    else:
        # variables for Vincent's method
        n_periods = total_useful_turns + 1
        ndoubles = n_periods * args.nsectors_per_turn

    print("n periods: ", n_periods)
    print("n doubles: ", ndoubles)

    args.n_periods = n_periods
    args.ndoubles = ndoubles

    # checking maximum number of cpu
    # diving max /2 as the recon is done on 1/2 a machine
    if args.ncpus is not None:
        args.ncpus = int(args.ncpus)
    else:
        args.ncpus = 10    

    max_num_cpus = int(os.cpu_count() / 2)
    if args.ncpus > max_num_cpus:
        args.ncpus = max_num_cpus

    # preparing multiprocessing
    print(f"doing multi processing on {args.ncpus:02d} cpus")
    pool = multiprocessing.Pool(  int(args.ncpus) )
    
    if args.method == 1:
        #################### Method 1 - Alessandro ###########################
        arguments = []

        for i_cpu in range( args.ncpus):
             arguments.append( ( args, i_cpu     )     )


        pool.starmap( somma_over_distributed,    arguments[:]   ) 
        
        res = np.zeros((args.ndoubles,) + args.one_radio_shape, "d")
        weights = np.zeros((args.ndoubles,) , "d")

        
        # Combinging all resulting h5 files 
        for i_cpu in range(args.ncpus):
            res += h5py.File(f"partial_{i_cpu:03d}.h5","r")["data"][()]
            weights += h5py.File(f"partial_{i_cpu:03d}.h5","r")["weights"][()]

        for i, r in enumerate(res):
            # print(i, len(res))
            r = r / weights[i]
            if not args.do_median:
                res[i] = r / scipy.ndimage.gaussian_filter(r, [0,args.sigma_pix] )
            else:
                res[i] = 1 + r

            
        with h5py.File(f"{args.output_file}","w") as fw:
            fw["nsectors_per_turn"] = args.nsectors_per_turn
            fw["period_nturns"]     = args.period_nturns
            fw["double_flats"]      = res.astype("f")

    else:
        
        #################### Method 2 - Vincent ###########################
        # Creating folder for temporary files output
        # making a more unique tmp folder to avoid issues when changing parameters
        tmp_output_path = f"{os.getcwd()}/DOUBLE_FLATFIELD_TMP_{args.nsectors_per_turn}_{args.period_nturns}_{int(args.sigma_pix)}"
        os.makedirs(tmp_output_path, exist_ok=True)
        
    
        ##### STep 1 #####
        # Angular average of projections
        process_complete = check_completion('angular_avg_', args, tmp_output_path, ndoubles)

        if process_complete == 0:
            # preparing arguments for multiprocessing
            arguments = prepare_arguments(args, args.ncpus, tmp_output_path, total_useful_turns )
            pool.starmap( make_angular_average,    arguments[:]   ) 
            print("Distributed angular average done!\n")
        else:
            print("Distributed angular average previously done, skiping!\n")        

        ##### Step 2 #####
        # combining files from previous process
        process_complete = check_completion('angular_cat_avg', args, tmp_output_path, args.nsectors_per_turn)
        if process_complete == 0:
            concatenate_results(args, total_useful_turns, tmp_output_path)
            print("Combination of temporary files from distributed angular average done!\n")   
        else:
            print("Combination of temporary files from distributed angular average already done, Skipping!\n")   
        prefix = 'angular_cat_avg'
 
        ##### Step 3 #####
        # computing vertical median        
        process_complete = check_completion('angular_vert_med', args, tmp_output_path, args.nsectors_per_turn)
        if process_complete == 0:
            # resetting arguments for further multiprocessing
            # no need for more cpu than nsectors per turn
            arguments = prepare_arguments(args, args.nsectors_per_turn, tmp_output_path, prefix)
            pool.starmap( vertical_axis_processing ,    arguments[:]   ) 
            print("Vertical median done!\n")
        else:
            print("Vertical median already done, skipping!\n")
        prefix = 'angular_vert_med'

        ##### Step 4 - optional #####
        # computing multi angle median  
        if args.multi_angle_med == 1:
            # computing multi angle image
            process_complete = check_completion('angular_med_rot', args, tmp_output_path, args.nsectors_per_turn)
            if process_complete == 0:
                # resetting arguments for further multiprocessing
                arguments = prepare_arguments(args, args.ncpus, tmp_output_path, prefix)
                pool.starmap( process_multi_angle_median ,    arguments[:]   )
                print("Multi angle median done!\n")
            else:
                print("Multi angle median already done skiping!\n")
            prefix = 'angular_med_rot'

        ##### Step 5 - optional #####
        # computing segmentation from defect map
        if args.segmentation_threshold > 0:
            process_complete = check_completion('angular_threshold_vert_med', args, tmp_output_path, args.nsectors_per_turn)
            if process_complete == 0 and args.force_overwrite:
                print(" Computing Defect Map")
                DefectMap = get_defect_map(args, tmp_output_path)
                print(" - Defect map done!")
                arguments = prepare_arguments(args, args.ncpus, tmp_output_path, prefix, DefectMap)
                pool.starmap( segmentation_with_map ,    arguments[:]   )  
            prefix = "angular_threshold_vert_med"

        ##### Step 6 #####
        # combining final results
        combining_all_angles(args, tmp_output_path, prefix)

    print('\nDouble Flatfield computation done!\n')
    return        

def somma_over_distributed(args, i_cpu  ):

    print(" distributed")
    f_scan = h5py.File(args.scan_file, "r")
    args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]

    sigma_degree = 360 * 0.7 / args.nsectors_per_turn
    print("sigma_degree: ", sigma_degree)    
        
    theta_start = args.angular_margin +  (  i_cpu / args.ncpus  )    * args.n_periods * args.period_nturns   *  360  
    theta_end   = args.angular_margin + (i_cpu + 1 ) / args.ncpus  * args.n_periods * args.period_nturns * 360  
    print("theta start: ", theta_start," theta_end ",theta_end) 

    iproj_start = np.searchsorted(args.angles, theta_start)
    iproj_end = np.searchsorted(args.angles, theta_end)

    results = np.zeros((args.ndoubles,) + args.one_radio_shape, "d")
    weights = np.zeros((args.ndoubles,), "d")    

    count_for_reading_limit = 0
    for iproj in range(iproj_start, iproj_end, args.proj_step):

        
        if ( iproj - iproj_start )  % 500 == 0:
            print(" iproj ", iproj , "i_cpu ", i_cpu,    iproj_start, iproj_end, theta_start, theta_end)
        
        angle = args.angles[iproj]        

        radio = None # if needed it will be read

        # loop over number of periods (total nb turn / nb of turn to consider in a period)
        for i_period in range(args.n_periods):
            
            # loop per angular sector requested
          for i_coeff in range(  0 ,  args.nsectors_per_turn ):
            
            for relative_turn in range(args.period_nturns):                
                coeff_angle = (
                    args.angular_margin
                    + (
                        i_period * args.period_nturns
                        + i_coeff / args.nsectors_per_turn
                        + relative_turn
                    )
                    * 360
                )
                # I think this was a debug print, it could be removed in the future                
                dist = abs(angle - coeff_angle)
                if  dist  < 3 * sigma_degree:                    
                    if radio is None:
                        radio = get_radio_or_flat(args, iproj)

                        count_for_reading_limit += 1
                        if count_for_reading_limit % 50 == 0 :
                            # to avoid too many open files problem
                            args.d_radios = None
                            args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]
                        
                        if args.do_median:
                            radio_mean = np.zeros_like(radio)
                            gpu_med.gpu_med( radio, radio_mean, int(args.sigma_pix) * 2 +1 , int(args.sigma_pix) * 2 +1   )
                            
                            radio = (radio - radio_mean) / radio_mean

                        
                    peso = math.exp(-dist * dist / sigma_degree / sigma_degree / 2)
                    weights[ i_period * args.nsectors_per_turn +  i_coeff ] += peso 
                    results[ i_period * args.nsectors_per_turn +  i_coeff ] += peso * radio

    with h5py.File(f"partial_{i_cpu:03d}.h5","w") as fw:
        fw["data"]    = results
        fw["weights"] = weights



def make_angular_average(args, i_cpu, output_path, total_useful_turns  ): 

    if i_cpu == 0:
        print("Distributed angular average")

    f_scan = h5py.File(args.scan_file, "r")
    args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"] 

    # calculation of the fraction angle handled by each CPU
    # and associated projection star and end
    # args.n_periods is removed from calculation here as we calculate images for each turn
    theta_start = args.angular_margin +  (  i_cpu / args.ncpus  )  * total_useful_turns * 360  
    theta_end   = args.angular_margin + (i_cpu + 1 ) / args.ncpus  * total_useful_turns * 360       

    iproj_start = np.searchsorted(args.angles, theta_start)
    iproj_end = np.searchsorted(args.angles, theta_end)

    # these print are just for debuging
    if i_cpu == 0:
        print(f" - Example of range for cpu: {i_cpu:02d}")
        print("   theta start: ", theta_start," theta_end ",theta_end) 
        print("   iproj_start: ",iproj_start ,"iproj_end:  ",iproj_end)     

    # creating an array representing the angular sectors per turn
    # the + 1 at the end is because we work with sector, i.e., intervals
    # so if there is a static 360 scan before the helical one, they are both
    # considered as being the same turn (stage is calculated with angle / 360)
    angle_array = np.linspace(0, 360, args.nsectors_per_turn+1)    
    
    for i_angle_array in angle_array[:-1]:
        # reseting flag variables
        new_image = 1       
        store_image = 0
        AVG_count = 0
        save_image = 0
        count_for_reading_limit = 0

        # defining matrices for saving results
        # stage is the position in the vertical series (1 turn = 1 stage)
        # count is used for the averaging when calculation is spread over 2 CPU
        # angle sector indicates where the averaging is done in a 360 rotation, the value indicated is the start of the angular fraction
        results = np.zeros((1 ,) + args.one_radio_shape, "d")        
        stages = np.zeros((1 ,), "d")   
        counts = np.zeros((1 ,), "d")   
        angle_sector = np.zeros((1 ,), "d")           

        for iproj in range(iproj_start, iproj_end, args.proj_step):                       
            # checking position within the 360 deg rotation
            angle = args.angles[iproj]
            mod_angle = angle % 360        
            stage_index = math.floor(angle / 360)
            valid_indices = np.where(angle_array <= mod_angle)[0]

            # Check if there are valid indices
            if valid_indices.size > 0:
                # Get the index of the maximum value among the valid indices
                closest_index = valid_indices[-1]
                closest_value = angle_array[closest_index]
            else:
                closest_index = None
                closest_value = None            

            if closest_value == i_angle_array:

                # reading radio in the angular sector                
                radio = get_radio_or_flat(args, iproj)

                count_for_reading_limit += 1
                if count_for_reading_limit % 50 == 0 :
                    # to avoid too many open files problem
                    args.d_radios = None
                    args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]                         

                if new_image == 1:
                    # defining first_im for output filename
                    first_im = iproj
                    store_image = 1
                    new_image = 0
                    if ( iproj - iproj_start )  % math.ceil(args.last_to_be_summed/20) == 0:
                        print(f" - proj: {iproj:.0f} angle: {angle:.1f}, mod: {mod_angle:.1f}, in sector: {closest_value}-{angle_array[closest_index+1]}, stack index {stage_index}, icpu: {i_cpu:02d}")                    
                    stages[0] = stage_index

                    # creating high pass filtered image
                    acc_radio = radio - medfilt_rapid(radio, filter_size=(args.sigma_pix, args.sigma_pix), padding_rapid='replicate')
                else:
                    # accumulating high pass filtered image
                    acc_radio += radio - medfilt_rapid(radio, filter_size=(args.sigma_pix, args.sigma_pix), padding_rapid='replicate')
                
                # counting processed images
                AVG_count +=1    

                if iproj + args.proj_step >= iproj_end:
                    # case of the last projection in the process
                    save_image=1
                    last_im = iproj
            else:
                if store_image ==1:
                    # projection not in the angular sector
                    # but previous one was and needs to be saved
                    # last_im is used in output filename
                    save_image = 1
                    last_im = iproj -1
                else:
                    #image is not in valid angular sector
                    # and previous images was not stored
                    save_image = 0

                # reseting variable as we are not in a valid angular fraction
                store_image = 0
                new_image = 1

            if save_image ==1 :    
                # reseting variable 
                save_image = 0
                
                # saving results           
                results[0] = acc_radio
                counts[0] = AVG_count
                angle_sector[0] = i_angle_array  

                # reseting to variable
                AVG_count = 0                               
                
                filename = f"{output_path}/angular_avg_{first_im:07d}-{last_im:07d}-{i_cpu:02d}.h5"                     
                with h5py.File(filename,"w") as fw:
                    fw["data"]   = results
                    fw["stages"] = stages 
                    fw["weight"] = counts
                    fw["angle_sector"] = angle_sector



def concatenate_results(args, total_useful_turns, output_path):
    print("Combining temporary files from distributed angular average (showing progress every 10% increment)")

    # getting list of files to process
    matching_files = make_list_of_files(prefix = 'angular_avg', wdir = output_path)

    #defining 10% of the files list length to limit display
    number_of_files = len(matching_files)
    ten_perc = math.ceil(number_of_files / 10)    
    
    # preparing arrays for storing results
    angle_array = np.linspace(0, 360, args.nsectors_per_turn+1)
    results = []
    stages = []
    angle_sectors = []
    weights = []

    for i_angle_array in angle_array[:-1]:
        # creating an array of array allows dispatching each previously angular avg image in a correct position
        results.append(np.zeros((total_useful_turns ,) + args.one_radio_shape, "d")) 
        stages.append(np.zeros((total_useful_turns ,), "d")) 
        angle_sectors.append(np.zeros((total_useful_turns ,), "d")) 
        weights.append(np.zeros((total_useful_turns ,), "d")) 

    for idx, file in enumerate(matching_files):
        # going through list of files        
        with h5py.File(file, 'r') as h5file:            
            # reading all entries in h5 file
            angle_sector = h5file['angle_sector'][0]
            stage = int(h5file['stages'][0])
            weight1 = int(h5file['weight'][()])
            data = h5file['data'][:]

        # converting angular franction position into an index for storing image
        index = int(np.where(angle_array == angle_sector)[0])
        if idx  % ten_perc == 0:
            print(f"    + File {os.path.basename(file)} is sector {angle_sector}, item {index}, weight {weight1} and stage {stage}")
        
        # Getting previous weight and image (previous weight=0 when it's the first image added)
        weight0 = weights[index][stage]
        im0 = results[index][stage]         

        # 'data' is already the result of im1 x weight1
        avg_weight = weight0 + weight1
        res = ((im0 * weight0) + data) / avg_weight

        # storing results
        results[index][stage] += res[0]
        weights[index][stage] = avg_weight        
        stages[index][stage] = stage
        angle_sectors[index][stage] = angle_sector
        
    print(" - Writing results")
    for index, (data, stage, angle, weight) in enumerate(zip(results, stages, angle_sectors, weights)):        
        # storing images, creating 1 h5 per angular fraction, each containing all vertical stages.
        filename = f"{output_path}/angular_cat_avg{int(angle_array[index]):03d}-{int(angle_array[index+1]):03d}.h5"
        with h5py.File(filename,"w") as fw:
            fw["data"] = data
            fw["stages"] = stage             
            fw["angle_sectors"] = angle
            fw["weight"] = weight



def process_multi_angle_median(args, i_cpu, output_path, prefix):
    # multi angular median removes linear features and keeps only blobs
    if i_cpu ==0:
        print("Starting processing multi angle median")

    # making list of files to process
    matching_files = make_list_of_files(prefix, wdir = output_path)

    if i_cpu is 0:
        print(f" - Example of files list for cpu {i_cpu:02d}")
        print(f"   Doing multi angle median on {os.path.basename(matching_files[0])}...{os.path.basename(matching_files[-1])}")

    # splitting files into sub-array for each i_cpu
    split_arrays = get_reference_split_array(args)

    for index in split_arrays[i_cpu]:
        if i_cpu ==0:
            print(f" - Cpu {i_cpu:02d} is doing med rot in {os.path.basename(matching_files[index])}")    

        file = matching_files[index]
        with h5py.File(file, 'r') as h5file:            
            data = h5file['data'][:]            
            angle_sector = h5file['angle_sectors'][:]
            stage = h5file['stages'][:]

        data_mod = np.empty_like(data)

        for index in range(data.shape[0]):
            # applying multi angle median filter
            data_mod[index] = multi_directional_median(data[index], args.sigma_pix)
        
        # saving results into a new file
        new_filename = file.replace(prefix, "angular_med_rot")
        if i_cpu ==0:
            print(f"    + New file saved as {os.path.basename(new_filename)}")        
        with h5py.File(new_filename,"w") as fw:
            fw["data"] = data_mod
            fw["stages"] = stage             
            fw["angle_sectors"] = angle_sector 



def vertical_axis_processing(args, i_cpu, output_path, prefix):
    if i_cpu ==0:
        print("Starting processing vertical median")
    # making list of files to process
    matching_files = make_list_of_files(prefix = prefix, wdir = output_path)

    #showing example for cpu 1
    if i_cpu is 0:
        print(f" - Example of files list for cpu {i_cpu:02d}")
        print(f"   Doing multi angle median on {os.path.basename(matching_files[0])}...{os.path.basename(matching_files[-1])}")

    # splitting files into sub-array for each i_cpu
    split_arrays = get_reference_split_array(args)

    for index in split_arrays[i_cpu]:
        if i_cpu ==0:
            print(f"  + Cpu {i_cpu:02d} is vertical median on  {os.path.basename(matching_files[index])}")    

        file = matching_files[index]
        with h5py.File(file, 'r') as h5file:            
            data = h5file['data'][:]
            angle_sector = h5file['angle_sectors'][:]
            stage = h5file['stages'][:]            
            
        # applying median sliding window over requested number of z stages
        if args.multi_angle_med>0:
            #forcing fixed window if using multi angle median as the latter
            #is quite computing heavy
            data_mod = median_fixed_window(data, window_size=args.period_nturns)
        else:
            data_mod = median_sliding_window(data, window_size=args.period_nturns)
        
        # saving results into a new file
        new_filename = file.replace(prefix, "angular_vert_med")
        if i_cpu ==0:
            print(f"    + New file saved as {os.path.basename(new_filename)}")        
        with h5py.File(new_filename,"w") as fw:
            fw["data"] = data_mod
            fw["stages"] = stage             
            fw["angle_sectors"] = angle_sector   



def segmentation_with_map(args, i_cpu, output_path, prefix, defectMap):
    if i_cpu ==0:
        print(f" - Starting processing thresholding on files starting with {prefix}")
    # making list of files to process
    matching_files = make_list_of_files(prefix = prefix, wdir = output_path)

    #showing example for cpu 1
    if i_cpu is 0:
        print(f" - Example of files list for cpu {i_cpu:02d}")
        print(f"   Applying defect map on {os.path.basename(matching_files[0])}...{os.path.basename(matching_files[-1])}")

    # splitting files into sub-array for each i_cpu
    split_arrays = get_reference_split_array(args)

    for index in split_arrays[i_cpu]:
        if i_cpu ==0:
            print(f"     + Cpu {i_cpu:02d} is thresholding  {os.path.basename(matching_files[index])}")    

        file = matching_files[index]
        with h5py.File(file, 'r') as h5file:            
            data = h5file['data'][:]
            angle_sector = h5file['angle_sectors'][:]
            stage = h5file['stages'][:]            
            
        # applying median sliding window over requested number of z stages        
        data_mod = np.empty_like(data)
        mask = np.empty_like(data)
        for index in range(data.shape[0]):                       
            mask[index] = defectMap
            data_mod[index] = data[index] * defectMap
        
        # saving results into a new file
        new_filename = file.replace(prefix, "angular_threshold_vert_med")
        if i_cpu ==0:
            print(f"    + New file saved as {os.path.basename(new_filename)}")        
        with h5py.File(new_filename,"w") as fw:
            fw["data"] = data_mod
            fw["mask"] = mask
            fw["stages"] = stage             
            fw["angle_sectors"] = angle_sector   


def get_defect_map(args, output_path ):
    #`Defining name to check if file exist
    Defect_Map_Name = f"{output_path}/DefectMap.h5" 

    if os.path.isfile(Defect_Map_Name) and not args.force_overwrite:
        # file exist, reading it
        print(f"The file '{os.path.basename(Defect_Map_Name)}' exists and is a file.")
        with h5py.File(Defect_Map_Name, 'r') as h5file:
            DefectMap = h5file['defectMap'][:]   
    else:
        # file does not exist, creating it
        print(f"The file '{os.path.basename(Defect_Map_Name)}' does not exist computing it now.")

        # computing defect map from flatfield image
        mid_range = math.ceil(args.last_to_be_summed / 2)
        flat = get_radio_or_flat(args, mid_range, get_flat=True)

        DefectMap = make_defect_map(Defect_Map_Name, flat, args.sigma_pix, 3, args.segmentation_threshold)
    return DefectMap

 

def combining_all_angles(args, output_path, prefix):
    # combining previous created images into a singel file
    # getting list of files to process
    matching_files = make_list_of_files(prefix = prefix, wdir = output_path)

    # preparing result files
    res = np.zeros((args.ndoubles,) + args.one_radio_shape, "d")

    # angle array for referencing angle fractions as indexes
    angle_array = np.linspace(0, 360, args.nsectors_per_turn+1)

    for file in matching_files:
        print(f" - Processing {os.path.basename(file)}")
        with h5py.File(file, 'r') as h5file:            
            data = h5file['data'][:]            
            angle_sectors = h5file['angle_sectors'][:]
            stages = h5file['stages'][:]

        for index, (data, stage, angle) in enumerate(zip(data,stages,angle_sectors)): 
            angle_index = int(np.where(angle_array == angle)[0])            
            res_index = int(angle_index + (stage * args.nsectors_per_turn))
            #print(f"    + image with angle {angle} (index angle {angle_index}) and stage {stage} goes in position {res_index}")
            res[res_index] += data
            

    # saving results into a single file
    # with this method period_nturns is always 1
    print(f" - Writing final results in {args.output_file}")        
    if args.multi_angle_med > 0:
        period_nturns = args.period_nturns
    else:
        period_nturns = 1
    args.period_nturns
    with h5py.File(f"{args.output_file}","w") as fw:
        fw["nsectors_per_turn"] = args.nsectors_per_turn
        fw["period_nturns"]     = period_nturns
        fw["double_flats"]      = res.astype("f")


def prepare_arguments(args, max_njobs, *extra_args):
    # reseting arguments list
    arguments = []

    # for some process, we won't want more jobs than files to process
    if args.ncpus > max_njobs:
        njobs = max_njobs
    else:
        njobs = args.ncpus

    for i_cpu in range(njobs):
        # Create the tuple to be appended by combining args, i_cpu, and extra_args
        argument_tuple = (args, i_cpu) + extra_args
        arguments.append(argument_tuple)
    return arguments



def check_completion(prefix, args, wdir, expected_nb_files):
    # checking expected number of files to see if the process was done
    process_complete = 0
    matching_files = make_list_of_files(prefix, wdir)
    print(f"Checking possible previous process with radix {prefix}:\nFound {len(matching_files)} previous results files vs {expected_nb_files} expected")
    if len(matching_files) >= expected_nb_files:
        process_complete =1
    return process_complete



def make_list_of_files(prefix, wdir):
    #print(f"listing files starting with {prefix}")
    # List to store matching files with a given prefix
    matching_files = []

    # Iterate over the files in the directory
    for filename in os.listdir(wdir):
        if filename.startswith(prefix):            
            matching_files.append(os.path.join(wdir, filename))
            
    return matching_files



def get_reference_split_array(args):
    # this splits the number of files to process
    angle_index_array = np.linspace(0, args.nsectors_per_turn - 1, args.nsectors_per_turn, dtype=int)    

    # Step 2: Automatically determine the lengths of each of the args.ncpus arrays
    total_elements = args.nsectors_per_turn
    num_arrays = args.ncpus
    base_length = total_elements // num_arrays
    extra_elements = total_elements % num_arrays

    # Create lengths array
    lengths = [base_length + 1 if i < extra_elements else base_length for i in range(num_arrays)]

    # Check if the sum of lengths is equal to the length of the linspace array
    assert sum(lengths) == args.nsectors_per_turn, f"The sum of lengths must be equal to {args.nsectors_per_turn}."

    # Step 3: Split the array according to the calculated lengths
    split_arrays = np.split(angle_index_array, np.cumsum(lengths)[:-1])
    return split_arrays



# Function to compute a sliding median on stack
def median_sliding_window(images, window_size=4):
    num_images, height, width = images.shape
    medians = np.zeros((num_images, height, width))
    
    for i in range(num_images):
        # Determine the start and end of the window
        start = max(0, i - window_size // 2)
        end = min(num_images, i + window_size // 2 + 1)

        # forcing window to always include the same number of images
        if start == 0:
            end = min(num_images, window_size)
        elif end == num_images:
            start = max(0, num_images - window_size)
        
        # Compute the median of the window
        window = images[start:end]
        medians[i] = np.median(window, axis=0)
    
    return medians



# Function to compute a median on stack (no sliding window)
def median_fixed_window(images, window_size=4):
    num_images, height, width = images.shape
    num_windows = num_images // window_size
    medians = np.zeros((num_windows, height, width))
    
    for i in range(num_windows):
        # Determine the start and end of the window
        start = i * window_size
        end = start + window_size

        # Compute the median of the window
        window = images[start:end]
        medians[i] = np.median(window, axis=0)
    
    return medians



# function to speed up median filter calculation
def medfilt_rapid(im, filter_size=(3, 3), padding_rapid='replicate'):
    repetition = 2  # number of passes to avoid aliasing
    speed_factor = 3  # general acceleration factor (important for large filter sizes)

    HS = int(filter_size[0])
    VS = int(filter_size[1])
    
    if padding_rapid == 'replicate':
        im2 = cv2.copyMakeBorder(im, HS-1, HS-1, VS-1, VS-1, cv2.BORDER_REPLICATE)
        padding = 'reflect'
    else:
        im2 = im
        padding = padding_rapid

    X2, Y2 = im2.shape

    Hacceleration = max(np.log(HS) * speed_factor, 1)
    Vacceleration = max(np.log(VS) * speed_factor, 1)

    if HS > 3:
        HS_size = int(np.floor(HS / Hacceleration / 2) * 2 + 1)
    else:
        HS_size = HS

    if VS > 3:
        VS_size = int(np.floor(VS / Vacceleration / 2) * 2 + 1)
    else:
        VS_size = VS

    Vrescale = min(1 / (VS / VS_size), 1)
    Hrescale = min(1 / (HS / HS_size), 1)

    for _ in range(repetition):
        im2 = cv2.resize(im2, (int(Y2 * Vrescale), int(X2 * Hrescale)), interpolation=cv2.INTER_CUBIC)
        im2 = scipy.ndimage.median_filter(im2, size=(HS_size, VS_size), mode=padding)
        im2 = cv2.resize(im2, (Y2, X2), interpolation=cv2.INTER_CUBIC)

    if padding_rapid == 'replicate':
        im2 = im2[HS-1:HS-1+im.shape[0], VS-1:VS-1+im.shape[1]]

    return im2



def multi_directional_median(Im, sigma_pix):
    # Applying 1D median filter, rotating image by 10 degrees increment
    for angle in range(0, 180, 10):
        #print(f'Processing the angle {angle}')
        
        # Rotating image
        im_rot = scipy.ndimage.rotate(Im, -angle, reshape=True)        
        
        # Applying 1D median filter twice for better results
        im_med = medfilt_rapid(im_rot, filter_size=(1, sigma_pix * 2), padding_rapid='replicate')
        im_med = medfilt_rapid(im_med, filter_size=(1, sigma_pix * 2), padding_rapid='replicate')
        
        # Returning image to original angle
        im_rot_back = scipy.ndimage.rotate(im_med, angle, reshape=True)    
        
        # Cropping image back to original size
        dim1_diff = im_rot_back.shape[0] - Im.shape[0]
        dim2_diff = im_rot_back.shape[1] - Im.shape[1]
        
        if dim1_diff > 0 and dim2_diff == 0:
            c = im_rot_back[round(dim1_diff / 2):round(dim1_diff / 2) + Im.shape[0], :]
        elif dim1_diff > 0 and dim2_diff > 0:
            c = im_rot_back[round(dim1_diff / 2):round(dim1_diff / 2) + Im.shape[0], 
                            round(dim2_diff / 2):round(dim2_diff / 2) + Im.shape[1]]
        elif dim1_diff == 0 and dim2_diff > 0:
            c = im_rot_back[:, round(dim2_diff / 2):round(dim2_diff / 2) + Im.shape[1]]
        else:
            c = im_rot_back
        
        # Subtracting result of 1D median filter to original image
        Im = Im - c
    
    return Im



def blurring_rapid(image, kernel_size):
    return cv2.GaussianBlur(image, (kernel_size, kernel_size), 0)



def make_defect_map(Defect_Map_Name, Im, DefectSize, Smoothing, imThreshold, defectsLinearCorr=0):

    ref_defects = Im.copy()
    # Global defects
    #print("high pass filter")
    ref_defects = np.abs(ref_defects - medfilt_rapid(ref_defects, (DefectSize, DefectSize)))

    #print("keeping only positive values")
    ref_defects = np.maximum(0, ref_defects - np.mean(medfilt_rapid(ref_defects, (DefectSize, DefectSize))))

    #print("removing isolated point")
    ref_defects = medfilt_rapid(ref_defects, (Smoothing, Smoothing))   

    #print("thresholding")
    percentile = np.percentile(ref_defects, 100 - (imThreshold * 100))    
    ref_defects = (ref_defects > percentile)
    
    
    #ref_defects = np.minimum(np.maximum(ref_defects - imThreshold, 0) * 1e20, 1)

    if defectsLinearCorr > 0:
        ref_defects = ref_defects.astype(np.float64) - medfilt_rapid(ref_defects.astype(np.float64), (1, defectsLinearCorr))

    SE = disk(3)
    #print("Dilate")
    ref_defects = cv2.dilate(ref_defects.astype(np.float64), SE)

    #print("Bluring")
    # blurring rapid was a matlab function that was reducing the size of the image
    # I had a placeholder function for it but it seems it's unecessary as the
    # the Gaussiant blur is fast enought. I will keep the placeholder just in case
    #ref_defects = blurring_rapid(ref_defects, 3)
    ref_defects = cv2.GaussianBlur(ref_defects, (3, 3), 0)

    # Second map only for white defects
    ref_defects_max = Im.copy()

    #print("HF2")
    ref_defects_max = ref_defects_max - medfilt_rapid(ref_defects_max, (DefectSize, DefectSize))

    #print("neg val")
    ref_defects_max = np.maximum(0, ref_defects_max - np.mean(medfilt_rapid(ref_defects_max, (DefectSize, DefectSize))))

    #print("removing isolated point")
    ref_defects_max = medfilt_rapid(ref_defects_max, (Smoothing, Smoothing))

    percentile = np.percentile(ref_defects_max, 100 - (imThreshold * 100))    
    ref_defects_max = (ref_defects_max > percentile)  
    #ref_defects_max = np.minimum(np.maximum(ref_defects_max - imThreshold, 0) * 1e20, 1)

    if defectsLinearCorr > 0:
        ref_defects_max = ref_defects_max.astype(np.float64) - medfilt_rapid(ref_defects_max.astype(np.float64) , (1, defectsLinearCorr))

    #print("Dilating")
    SE = disk(5)
    ref_defects_max = cv2.dilate(ref_defects_max.astype(np.float64), SE)

    #print("Bluring")
    #ref_defects_max = blurring_rapid(ref_defects_max , 3)
    ref_defects_max = cv2.GaussianBlur(ref_defects_max, (3, 3), 0)

    #print("final map")
    defectMap = np.clip(ref_defects + ref_defects_max, 0, 1)
    #defectMap = np.minimum(ref_defects + ref_defects_max, 1)
    
    # I'm saving all the intermediate images to help seeing if the code does what I want
    print(f"   + Writing final results in {Defect_Map_Name}")
    with h5py.File(f"{Defect_Map_Name}","w") as fw:
        fw["defectMap"] = defectMap
        fw["ref_defects"] = ref_defects
        fw["ref_defects_max"] = ref_defects_max
        fw["flat"] = Im

    return defectMap

if __name__ == "__main__":#
    main()
