 
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <vector>
#include <tuple>

#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<string>
#include<math.h>

#define FORCE_IMPORT_ARRAY

#include<string>
#include <pybind11/numpy.h>
#include<exception>
#include"utilities.h"


void gpu_med( int ny, int nx, float *data, float *result, int hwy, int hwx, float threshold) ;


using namespace pybind11::literals;  // to bring in the `_a` literal, they are used to pass arguments by keyword


// ----------------
// Python interface
// ----------------

namespace py = pybind11;

PYBIND11_MODULE(gpu_med,m)
{
  // xt::import_numpy();
  m.doc() = "nabu c/c++ acceleration for median filter";
  
  m.def("gpu_med", []( py::array_t<float> &input,
		       py::array_t<float> &output, 
		       int w_y,
		       int w_x
		       )
		   {
		     
		     py::buffer_info input_info  =  nabuxx::checked_buffer_request(input , 'f' , "filter of aspect_phase_retrieval" , "input",  nabuxx::memlayouts::c_contiguous, 2 );
		     py::buffer_info output_info  =  nabuxx::checked_buffer_request(output , 'f' , "filter of aspect_phase_retrieval" , "output",  nabuxx::memlayouts::c_contiguous, 2 );
		     
		     if(  input_info.shape[0] != output_info.shape[0] || input_info.shape[1] != output_info.shape[1]   ) {
		       std::stringstream ss;
		       ss << "The argumens input and output,  to filter,  must have  the same shape but their shaper are " <<  input_info.shape[0] << "," <<  input_info.shape[1]  ;
		       ss << " and " <<  output_info.shape[0] << "," <<  output_info.shape[1]  ;
		       throw std::invalid_argument(ss.str() );
		     }
		     
		     
		     gpu_med(
			     input_info.shape[0],
			     input_info.shape[1],
			     (float*) input_info.ptr, 
			     (float*) output_info.ptr,
			     (w_y-1)/2,
			     (w_x-1)/2,	    
			     -1.0e30
			     ) ;
		     return py::none();
		   },
	py::arg("data"),
	py::arg("output"),
	py::arg("w_y")=3 ,
	py::arg("w_x")=3 
	);
  
}

