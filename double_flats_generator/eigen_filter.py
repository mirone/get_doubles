from h5py import *
import numpy as np
import multiprocessing
import os


def get_arguments():
    parser = argparse.ArgumentParser(
        description="Filters the doubles using a principal component."
    )
    parser.add_argument(
        "--weight_file",
        required=True,
        help="The weight file, the weight is used to scale down importance of pathological region, when doing the scalar products",
    )
    parser.add_argument(
        "--doubles_file",
        required=True,
        help="The file containing the doubles that we want to filter",
    )
    parser.add_argument(
        "--tile_size",
        required=False,
        type=int,
        default=40,
        help="The size of the tiles",
    )

    
    args = parser.parse_args()
    
    return args



def main():

    args = get_arguments()
    
    w_map = File(args.weight_file,"r")["entry0000/weights_field/results/data"][()]
    
    with File(args.doubles_file ,"r") as f:
        dataset = f["double_flats"]
        h,w = dataset.shape[1:]
        print(" reading data ")
        data = dataset[()]
    
    tile_s = args.tile_size    
    print("reading OK ")

    if 'SLURM_CPUS_PER_TASK'  in  os.environ :
        ncpus = os.environ['SLURM_CPUS_PER_TASK'] 
    else:
        ncpus =  multiprocessing.cpu_count()
    print("POOL of ",  ncpus)
    
    pool = multiprocessing.Pool(  int(ncpus) )

    arguments = []
    
    for ph in range(0, h, tile_s):
        for pw in range(0, w, tile_s):
            arguments.append([data[:, ph:ph+tile_s, pw:pw+tile_s ], ph, pw,  w_map[ ph:ph+tile_s, pw:pw+tile_s ]])

    all_results = pool.starmap( transforma, arguments   ) 

    for result, ph, pw in all_results:
        data[:, ph:ph + tile_s   , pw:pw +tile_s ] =  result

    File("filtered_doubles.h5","w")["double_flats"] = data    

        
def transforma( dd , ph, pw, wmap) :
    print(ph, pw)
    h,sy,sx = dd.shape
    ddav= 1 # dd.mean(axis=0)
    dd=dd-ddav
    dd.shape = -1, sy*sx

    wmap = np.array(wmap)
    wmap.shape =  sy*sx

    wmap = np.sqrt(wmap)
    
    mm=np.zeros([sy*sx, sy*sx],"d" ) 


    max_list = np.array([tok.max() for tok in dd])
    min_list = np.array([tok.min() for tok in dd])

    diff_list = max_list - min_list

    order = np.arange( diff_list.shape[0] )[ np.argsort( diff_list  )  ]

    for i in order:
        if i < 1.1 * len(order):
            v = dd[ order[i]  ] * wmap
            mm[:] +=   v[:, None]*v


    eig = np.ones_like(mm[0])
    eig = eig / np.linalg.norm(eig)


    for i in range(80):
        old = eig
        eig = np.dot(mm,old)
        eig = eig / np.linalg.norm(eig)
    print(  np.linalg.norm(old - eig))

    
    eig.shape=sy,sx
    dd.shape = -1, sy,sx
    wmap.shape =  sy,sx

    results=np.zeros_like(dd)
    results += ddav

    eig=eig - eig.mean()
    for i in range(len(dd)):
        f = ( dd[i] * eig *wmap  ).sum()
        results[i] += f * eig / wmap

    return results, ph, pw


if __name__ == "__main__":
    main()
