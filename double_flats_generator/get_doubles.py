import argparse
import math
import os
import h5py
import numpy as np
import scipy
import scipy.ndimage
import scipy.signal
import multiprocessing
from   .  import gpu_med

def get_arguments():
    parser = argparse.ArgumentParser(
        description="Produces doubleflats as a function of time."
    )
    parser.add_argument(
        "--scan_file",
        required=True,
        help="the name root of the scan for which the double-flats are generated",
        default="",
    )
    parser.add_argument(
        "--output_file",
        required=True,
        help="The file containing the double-flats",
    )
    parser.add_argument(
        "--entry_name",
        required=False,
        help="The entry name",
        default="entry0000",
    )

    parser.add_argument(
        "--period_nturns",
        required=True,
        type=int,
        help="Doubles are produced for periods  n_turns wide",
    )

    parser.add_argument(
        "--sigma_pix",
        required=True,
        type=float,
        help="The sigma in pixels for the high-pass filter",
    )

    parser.add_argument(
        "--coeffs_per_period",
        required=False,
        type=int,
        default=1,
        help="",
    )
    parser.add_argument(
        "--stage",
        required=True,
        type=int,
        default=1,
        choices=(1,2),
        help="Give 1 for filtering, 2 for averaging",
    )
    parser.add_argument(
        "--output_dir_for_filtered",
        required=False,
        type=str,
        default="filtered_dir",
        help="The directory where filtered projections will be written",
    )
    parser.add_argument(
        "--ncpus",
        required=False,
        type=int,
        help="How many processes will be spawned. If not given, this number will be read from SLURM_CPUS_PER_TASK",
    )

    args = parser.parse_args()

    f_scan = h5py.File(args.scan_file, "r")
    f_flats = h5py.File(os.path.splitext(args.scan_file)[0] + "_flats.hdf5", "r")
    f_darks = h5py.File(os.path.splitext(args.scan_file)[0] + "_darks.hdf5", "r")

    control_keys = f_scan[f"/{args.entry_name}/instrument/detector/image_key"][()]
    args.angles = f_scan[f"/{args.entry_name}/sample/rotation_angle"][()]
    
    args.angles =   args.angles [np.equal(0, control_keys)]
    
    args.angles = np.unwrap(
        args.angles, period=360
    )

    args.angles = abs(args.angles - args.angles[0])
    args.radios_indexes = np.arange(control_keys.shape[0])[np.equal(0, control_keys)]

    args.radios_current = f_scan[f"/{args.entry_name}/control/data"][()][
        np.equal(0, control_keys)
    ]

    g_flats = f_flats[f"/{args.entry_name}/flats"]

    args.flats_keys = [int(k) for k in g_flats.keys() if k.isnumeric()]

    args.flats_keys.sort()

    args.flats_current = g_flats["machine_electric_current"][()]

    g_darks = f_darks[f"/{args.entry_name}/darks"]
    for key in g_darks.keys():
        args.dark = g_darks[key][()]
        break

    args.flats = np.array(
        [
            (g_flats[str(k)][()] - args.dark) * 0.2 / args.flats_current[i]
            for i, k in enumerate(args.flats_keys)
        ]
    )

    args.good_ones = np.less_equal(360, args.angles) * np.less_equal(
        args.angles, args.angles[-1] - 360
    )

    args.first_to_be_summed = np.searchsorted(args.angles, margine_angolare)
    args.last_to_be_summed = min(  np.searchsorted(args.angles, args.angles[-1] - margine_angolare) , len(args.angles) - 1  ) 

    steps = np.abs(np.diff(args.radios_indexes))
    typical_step = np.median(steps)


    # ??
    args.flats_by_sector = False
    for i, d in enumerate(steps):
        if d > 2 * typical_step:
            
            args.flats_by_sector = True
            
            args.ndx_first_period = args.radios_indexes[i]
            args.ndx_period = args.ndx_first_period - args.radios_indexes[0]
            args.ndx_last_period = args.radios_indexes[i + 1]

    args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]
    #print(  get_radio_or_flat(args, 0).shape  )
    args.one_radio_shape = tuple(get_radio_or_flat(args, 0).shape)

    args.d_radios=None
    args.f_scan = None
    args.f_flats  = None
    args.f_darks  = None

    return args

    #  angles
    #  radios_current
    #  flats_keys
    #  flats_current
    #  flats
    #  dark
    #  control_indees
    #  d_radios
    #  radios_indexes


def get_radio_or_flat(args, i, get_flat=False):

    if (not get_flat) and (i >= args.radios_indexes.shape[0]):
        return None

    ndx = args.radios_indexes[i]

    if (
        (not args.flats_by_sector)
        or (ndx <= args.ndx_first_period)
        or (ndx >= args.ndx_last_period)
        or get_flat
    ):
        pos = np.searchsorted(args.flats_keys, ndx)

        if pos == 0:
            i1 = 0
            i2 = 0
            f1 = 1
            f2 = 0
        elif pos == len(args.flats_keys):
            i1 = len(args.flats_keys) - 1
            i2 = 0
            f1 = 1
            f2 = 0
        else:
            i1 = pos - 1
            i2 = pos

            key_i1 = args.flats_keys[i1]
            key_i2 = args.flats_keys[i2]

            f1 = (key_i2 - ndx) / (key_i2 - key_i1)
            f2 = (ndx - key_i1) / (key_i2 - key_i1)

        flat = f1 * args.flats[i1] + f2 * args.flats[i2]

        if get_flat:
            return flat

        radio = args.d_radios[ndx] - args.dark
        
        radio_current = args.radios_current[i]
        radio = radio * 0.2 / radio_current

        return (radio / flat).astype("f")
    else:

        # assuming ndx=i for concatenated scan with flats by sector
        pi1 = i
        while pi1 > args.ndx_first_period:
            pi1 -= args.ndx_period

        pi2 = i
        while pi2 < args.ndx_last_period:
            pi2 += args.ndx_period

        ff1 = get_radio_or_flat(args, pi1, get_flat=True)
        ff2 = get_radio_or_flat(args, pi2, get_flat=True)

        flat = (ff1 * (pi2 - i) + (i - pi1) * ff2) / (pi2 - pi1)

        radio = args.d_radios[ndx] - args.dark
        radio_current = args.radios_current[i]
        radio = radio * 0.2 / radio_current

        return (radio / flat).astype("f")

def main():

    args = get_arguments()

    total_useful_turns = 1 + int(
        math.ceil( 0.1 + 
            (args.angles[args.last_to_be_summed] - args.angles[args.first_to_be_summed])
            / 360
        )
    )

    
    if args.stage ==1 :
        ncpus=6
        print("POOL of ",  ncpus)
        pool = multiprocessing.Pool(  int(ncpus) )
        print("POOL created of ",  ncpus)
        arguments = []
        for i_tok in range(ncpus):
            arguments.append( ( args, i_tok, ncpus  )   )

        print("chiamo star map")
        filtered_files = pool.starmap( filter_radios,    arguments[:]   ) 
        # filtered_files = [ filter_radios(*args)  for args in    arguments[:]   ] 
        print("chiamo star map OK")
        join_all( args, filtered_files   )
        return 0
    else:
        ncpus=10
        n_periods = int(math.floor(total_useful_turns / args.period_nturns))
        ndoubles = n_periods * args.coeffs_per_period
        
        # ncpus = int(os.environ['SLURM_CPUS_PER_TASK'] or multiprocessing.cpu_count())
        pool = multiprocessing.Pool(  int(ncpus) )
        results = np.zeros((ndoubles,) + args.one_radio_shape, "d")
        weights = np.zeros((ndoubles,), "d")
        
        arguments = []
        for i_period in range(n_periods):
            arguments.append( ( args, i_period, results[i_period * args.coeffs_per_period  : (i_period + 1) * args.coeffs_per_period ]     )     )

        # print(" MAPPO " , args.angles)

        partial_results = pool.starmap( somma_over,    arguments[:]   ) 

        bottom = 0
        for r in partial_results:
            # print(" mean ", r.mean())
            results[bottom:bottom + r.shape[0]] = r
            bottom += r.shape[0]

        
        with h5py.File(args.output_file, "w") as f:
            f["double_flats"] = results
            f["period_nturns"] = args.period_nturns
            f["ncoeffs_per_period"] = args.coeffs_per_period



def somma_over(args, i_period, results  ):

    radios_file_name = os.path.join(args.output_dir_for_filtered, f"filtered_radios.h5")

    f_source = h5py.File(radios_file_name,"r")
    radios_source = f_source["stack"]
    
    sigma_degree = 360 * 0.7 / args.coeffs_per_period
    
    weights = np.zeros(results.shape, "d")
    
    meanradios  = np.zeros(results.shape     , "d")
    meanweights = np.zeros(results.shape[:1] , "d")

    # mean_kernel = np.ones([ int(args.sigma_pix) * 2 +1 , int(args.sigma_pix)*2 +1 ] , "f" )
    # mean_kernel_x = np.ones([ 1  , int(args.sigma_pix)*2 +1 ] , "f" )
    # mean_kernel_y = np.ones([ int(args.sigma_pix)*2 +1, 1 ] , "f" )

    
    mean_kernel = np.ones([ int(args.sigma_pix) * 2 +1 , int(args.sigma_pix) * 2 +1 ] , "f" )

    offset = i_period * args.coeffs_per_period *0
    
    #print(" IPERIOD ", i_period)

    theta_start = 360 + i_period * args.period_nturns * 360 - 5 * sigma_degree
    theta_end = 360 + (i_period + 1) * args.period_nturns * 360 + 5 * sigma_degree

    iproj_start = np.searchsorted(args.angles, theta_start)
    iproj_end = np.searchsorted(args.angles, theta_end)

    # print(" ESTREMI ", theta_start, theta_end  )
    
    for iproj in range(iproj_start, iproj_end):

        # if iproj % 50:
        #     continue
        
        if iproj % 200 == 0:
            print(" iproj ", iproj , "i period ", i_period,   iproj_start, iproj_end, theta_start, theta_end)
        
        angle = args.angles[iproj]

        # print(" ANGLE ", angle)

        radio = None # if needed it will be read

        for i_coeff in range(
            i_period * 0 * args.coeffs_per_period, (i_period * 0 + 1) * args.coeffs_per_period
        ):
            
            for relative_turn in range(args.period_nturns):

                coeff_angle = (
                    360
                    + (
                        i_period * args.period_nturns
                        + i_coeff / args.coeffs_per_period
                        + relative_turn
                    )
                    * 360
                )
                
                # print(" COEFF ANGLE ", coeff_angle   )
                
                dist = abs(angle - coeff_angle)

                if dist < 5 * sigma_degree:

                    # print( " results ", results[i_coeff - offset ].min(), results[i_coeff - offset ].max() , i_coeff - offset )

                    if radio is None:
                        radio = get_radio_or_flat(args, iproj)
          
                    peso = math.exp(-dist * dist / sigma_degree / sigma_degree / 2)


                    
                    weights[i_coeff - offset ] += peso 

                                        
                    results[i_coeff - offset] += peso * radio

    for i, r in enumerate(results):
        r = r / weights[i]
        # results[i] = 1 + ( r - scipy.ndimage.gaussian_filter(r, args.sigma_pix) ) / meanradios [i]
        results[i] = 1 + ( r )
        

    return results 

def join_all(args, filtered_files):
    _, (start, end) = filtered_files[-1]
    
    target_name = os.path.join(args.output_dir_for_filtered, "filtered_radios.h5")
    
    layout = h5py.VirtualLayout(shape=(end,) + args.one_radio_shape, dtype='f')

    for n, (filename, (start, end)) in  enumerate(filtered_files):
        shape = h5py.File(filename,"r")["stack"].shape
        vsource = h5py.VirtualSource(  os.path.relpath(filename,   os.path.dirname(target_name)), 'stack', shape=shape)
        layout[start:end] = vsource
            
    with h5py.File(target_name, 'w', libver='latest') as f:
        f.create_virtual_dataset('stack', layout, fillvalue=-5)


def filter_radios(args, i_tok, ntoks  ):


    f_scan = h5py.File(args.scan_file, "r")
    args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]


    os.makedirs( args.output_dir_for_filtered, exist_ok = True  )
    
    target_name = os.path.join(args.output_dir_for_filtered, f"filtered_radios_part_{i_tok:04d}.h5")

    n_radios = len( args.angles)

    n_my_radios = int(math.ceil( n_radios / ntoks  ))

    my_start = i_tok * n_my_radios
    my_end   = min( (i_tok+1) * n_my_radios, n_radios )

    n_my_radios = my_end-my_start

    print(" setting dataset ")
    f = h5py.File(target_name,  "w" )
    dset = f.create_dataset("stack",    (n_my_radios,) + args.one_radio_shape , dtype="f" )
    dset[-1,-1,-1]=0
    print(" setting dataset OK")
    f.close()
    
    f = h5py.File(target_name,  "r+" )
    dset = f["stack"]

    # for iproj in range(my_start, my_end):
    count_for_limit =0
    for iproj in range(my_start  , my_end):

        count_for_limit += 1

        if count_for_limit % 50 == 0:
            # to avoid too many open files problem
            args.d_radios = None
            args.d_radios = f_scan[f"/{args.entry_name}/instrument/detector/data"]

        
        if  (iproj % 200 == 0):
            print(" iproj ", iproj , "rel ", iproj - my_start,  n_my_radios ,  "i tok ", i_tok )

        radio = None # if needed it will be read

        radio = get_radio_or_flat(args, iproj)
        if radio is None:
            raise RuntimeError( f"no radio for {iproj} " ) 

        radio_mean=np.zeros_like(radio)
        gpu_med( radio, radio_mean, int(args.sigma_pix) * 2 +1 , int(args.sigma_pix) * 2 +1   )
        radio = (radio - radio_mean) / radio_mean

        dset[iproj-my_start] = radio

    f.close()
    f_scan.close()
    return  target_name, (my_start, my_end)


def bin_image(radio):
    sz, sx = [ tok - tok % 2 for tok in radio.shape  ]
    radio_view = radio[ :sz , :sx]
    radio_view.shape = sz//2, 2, sx//2, 2
    radio_binned = radio_view.mean(axis=(1,3))
    used_slices = slice(0,sz), slice(0,sx)
    return radio_binned, used_slices

def debin_image(radio):
    sz,sx = radio.shape
    result = np.zeros( (sz, 2, sx, 2), radio.dtype  )
    for i in range(2):
        for j in range(2):
            result[:,i,:,j] = radio
    result.shape = sz*2, sx*2
    return result


def get_cascaded_median(radio, sigma_pix):

    radio_binned = [None for i in range(5)]
    radio_median = [None for i in range(5)]
    used_slices = [None for i in range(5)]
    correction  = [None for i in range(5)]
    
    radio_binned[0] = radio
    
    for i in range(1,5):
        
        radio_binned[i], used_slices[i-1] = bin_image(radio_binned[i-1])

    for i in [4,3,2,1, 0]:
        radio_median[i] = np.zeros_like(   radio_binned[i] )
        if i<4:
            radio_binned[i][ used_slices[i]    ] -= debin_image(radio_median[i+1])
        gpu_med( radio_binned[i] , radio_median[i], int(sigma_pix) * 2 +1 , int(sigma_pix) * 2 +1   )
        
        if i<4:
            radio_median[i][ used_slices[i]    ] +=   debin_image(radio_median[i+1])
        
    return radio_median[0]
    


if __name__ == "__main__":
    main()
