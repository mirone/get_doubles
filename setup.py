# coding: utf-8

import os
from setuptools import setup, find_packages
from  find_pybind11_extensions import find_extensions, CMakeBuild
import os

packages=find_extensions()
print( packages)

def setup_package():
    doc_requires = [
        "sphinx",
        "cloud_sptheme",
        "myst-parser",
        "nbsphinx",
    ]
    setup(
        name="double_flats_generator",
        author="Alessandro Mirone",
        author_email="mirone@esrf.fr",
        maintainer="Alessandro Mirone",
        maintainer_email="mirone@esrf.fr",
        packages=find_packages(),
        package_data={
        },
        include_package_data=True,
        install_requires=[
            "numpy",
            "h5py",
            "scipy",
            "nabu",
        ],
        extras_require={
            "full": [
            ],
            "doc": doc_requires,
        },
        description="application to obtain double flat corrections",
        ext_modules=find_extensions(),
        cmdclass={"build_ext": CMakeBuild},
        entry_points={
            "console_scripts": [
                "get_double_flats     = double_flats_generator.get_doubles:main",
                "get_double_flats_mean     = double_flats_generator.get_doubles_mean:main",
                "filter_double_flats     = double_flats_generator.eigen_filter:main",
            ],
        },
        zip_safe=True,
    )
    
    
if __name__ == "__main__":
    setup_package()
